This is the front end for Imagineer, using Material Design Light and my library from npm, Imagineer.
Check it out at [here](http://petroff.ryan.gitlab.io/imagineerFrontend/)
http://petroff.ryan.gitlab.io/imagineerFrontend/


Future plans for Imagineer:
  - More dictionaries, including work-safe, christmas, sci-fi, etc
  - Installability like pokedex.org has http://www.pocketjavascript.com/blog/2015/11/23/introducing-pokedex-org https://github.com/nolanlawson/pokedex.org/tree/master/src/js
  - Somewhere nice and easy for users to submit new words or word lists
  - Reload-single-cards option
  - Make a presentation about the process of making this - it could be an okay conference talk!
